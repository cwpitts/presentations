"""
Initializes a Flask object and stores
it in the 'app' variable
"""
app = Flask(__name__)

# Defines a 'route' or endpoint
@app.route("/")
def index():
    return render_template("index.html")

# Run the app
if __name__ == "__main__":
    app.run(debug=True,
            port=5050,
            host="0.0.0.0")
