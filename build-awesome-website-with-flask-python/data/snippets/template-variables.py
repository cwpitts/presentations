@app.route("/template")
def templates_are_awesome():
    var="I replaced {{var1}}!"
    return render_template("variables.html",
                           var1=var,
                           listVar=["Item 1",
                                    "Item 2",
                                    "Item 3"])
