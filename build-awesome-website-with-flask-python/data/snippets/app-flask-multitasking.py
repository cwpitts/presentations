# All the methods!
@app.route("/resource",
           methods=["GET",
                    "POST",
                    "PUT",
                    "DELETE"])

def handle_request():
    if request.method == "GET":
        pass  # Return a resource
    elif request.method == "POST":
        pass  # Create a resource
    elif request.method == "PUT":
        pass  # Update a resource
    elif request.method == "DELETE":
        pass  # Delete a resource
