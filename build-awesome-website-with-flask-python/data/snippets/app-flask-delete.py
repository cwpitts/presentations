app = Flask(__name__)

@app.route("/delete", methods=["DELETE"])
def handle_delete():
    data = dict(request.get_json())
    # Delete the resource
    return jsonify(data)

if __name__ == "__main__":
    app.run(debug=True,
            port=5050,
            host="0.0.0.0")
