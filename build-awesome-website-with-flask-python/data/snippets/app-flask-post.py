app = Flask(__name__)

"""
Notice the additional argument in the
app.route decorator
"""
@app.route("/post", methods=["POST"])
def handle_post():
    data = dict(request.get_json())
    # Multivalue return sets return code
    return jsonify(data), 201

if __name__ == "__main__":
    app.run(debug=True,
            port=5050,
            host="0.0.0.0")
