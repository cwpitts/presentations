app = Flask(__name__)

@app.route("/put", methods=["PUT"])
def handle_put():
    data = dict(request.get_json())
    """
    Update the data or something awesome
    like that
    """
    return jsonify(data), 204

if __name__ == "__main__":
    app.run(debug=True,
            port=5050,
            host="0.0.0.0")
