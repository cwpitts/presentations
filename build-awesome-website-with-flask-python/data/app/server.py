#!/usr/bin/env python3
"""
This is a more fully-featured Flask server example
"""

# Main Flask components
from flask import (Flask,  # Flask itself
                   render_template,  # For rendering templates (clearly)
                   request,  # For accessing request data
                   jsonify)  # For sending a JSON response

app = Flask(__name__)
resources = {}

@app.errorhandler(404)
def page_not_found(e):
    return "I'm sorry lad, that page just isn't there...\n", 404

"""
This endpoint is for when we are reasoning
about resources as a collection
"""
@app.route("/resources", methods=["GET", "POST"])
def resource_endpoint_post():
    if request.method == "POST":
        data = dict(request.get_json())

        if "resource" in data:
            tmp = data["resource"]
    
            """
            This is idempotent, since we are checking
            to see if the resource already exists
            """
            if tmp["ID"] not in resources:
                resources[tmp["ID"]] = tmp["value"]

                return jsonify({
                    "response": {
                        "status": 201,
                        "description": "Resources created",
                        "current_resources": resources
                    }
                }), 201
            else:
                return jsonify({
                    "response": {
                        "status": 400,
                        "description": "Resource already exists",
                        "current_resources": resources
                    }
                }), 400
        else:
            return jsonify({
                "response": {
                    "status": 400,
                    "description": "Improperly formatted request"
                }
            }), 400
    elif request.method == "GET":
        return jsonify({
            "response": {
                "status": 200,
                "description": "Current resources returned",
                "current_resources": resources
            }
        }), 200


"""
This resource is for when we are reasoning
about a specific resources in the collection

This is a rather long method, so in a production
server it would probably be advisable to refactor
this into a collection of helper methods that
were invoked when the appropriate method was sent
(e.g. handle_resource_get, handle_resource_put, etc.)
"""
@app.route("/resources/<resourceID>", methods=["GET",
                                              "PUT",
                                              "DELETE"])
def resource_endpoint(resourceID):
    # Retrieve a resource
    if request.method == "GET":
        if resourceID in resources:
            return jsonify({
                "response": {
                    "status": 200,
                    "description": "Resource returned",
                    "resource": {
                        resourceID: resources[resourceID]
                    }
                }
            }), 200
    # Update a resource
    elif request.method == "PUT":
        data = dict(request.get_json())

        if resourceID not in data:
            return jsonify({
                "response": {
                    "status": 400,
                    "description": "Updated resource not in request",
                    "current_resources": resources
                }
            }), 400
        
        if resourceID in resources:
            resources[resourceID] = data[resourceID]
        
            return jsonify({
                "response": {
                    "status": 204,
                    "description": "Resources updated",
                    "current_resources": resources
                }
            }), 204
        else:
            return jsonify({
                "response": {
                    "status": 400,
                    "description": "Resource does not exist",
                    "current_resources": resources
                }
            }), 400
    # Delete a resource
    elif request.method == "DELETE":
        if resourceID in resources:
            del resources[resourceID]

            return jsonify({
                "response": {
                    "status": 200,
                    "description": "Resources deleted",
                    "current_resources": resources
                }
            }), 200
        else:
            return jsonify({
                "response": {
                    "status": 400,
                    "description": "Resource does not exist",
                    "current_resources": resources
                }
            }), 400


@app.route("/")
def index():
    return render_template("index.html")


if __name__ == "__main__":
    app.run(debug=True, port=5050, host="0.0.0.0")
