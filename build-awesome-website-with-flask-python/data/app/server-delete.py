#!/usr/bin/env python3

# Main Flask components
from flask import (Flask,  # Flask itself
                   render_template,  # For rendering templates (clearly)
                   request,  # For accessing request data
                   jsonify)  # For sending a JSON request
from flask_moment import Moment  # For inserting current time into pages

app = Flask(__name__)
moment = Moment(app)

userData = {}


@app.route("/delete", methods=["DELETE"])
def handle_delete():
    data = list(request.get_json())
    for resource in data:
        if resource in userData:
            del userData[resource]
    return jsonify({
        "response": "resources deleted!",
        "resources": userData
    })


@app.route("/put", methods=["PUT"])
def handle_put():
    data = dict(request.get_json())
    for key, value in data.items():
        if key in userData:
            userData[key] = value
    return jsonify({
        "response": "resources updated!",
        "resources": userData
    }), 204


@app.route("/post", methods=["POST"])
def handle_post():
    data = dict(request.get_json())

    for key, value in data.items():
        userData[key] = value

    return jsonify({
        "response": "resources created!",
        "resources": userData
    }), 201


@app.route("/")
def index():
    return render_template("index.html")


if __name__ == "__main__":
    app.run(debug=True, port=5050, host="0.0.0.0")
