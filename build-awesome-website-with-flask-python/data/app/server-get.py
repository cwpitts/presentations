#!/usr/bin/env python3

# Main Flask components
from flask import (Flask,  # Flask itself
                   render_template,  # For rendering templates (clearly)
                   request,  # For accessing request data
                   jsonify)  # For sending a JSON response
from flask_moment import Moment  # For inserting current time into pages

app = Flask(__name__)
moment = Moment(app)


@app.route("/")
def index():
    return render_template("index.html")


if __name__ == "__main__":
    app.run(debug=True, port=5050, host="0.0.0.0")
