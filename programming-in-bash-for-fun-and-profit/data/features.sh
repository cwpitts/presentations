#!/bin/bash

# Dynamic typing

# It's a string!
var="foobar"
printf "The value of var is %s\n" "${var}"

# Or an int?
var=7
printf "The value of var is %d\n" "${var}"

# Or a string?
printf "The value of var is %s\n" "${var}"
