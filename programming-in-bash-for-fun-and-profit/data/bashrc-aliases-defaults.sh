# Always use the -p for mkdir
alias mkdir='mkdir -p'

# Always use -l, -h, -a for ls
alias ls='ls -lah'
