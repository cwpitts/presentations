#!/bin/bash

declare -a arr=("manjaro" "arch" "ubuntu" "fedora")
filename="example.txt"

for i in ${arr[@]}
do
    printf "%s\n" "${i}" >> ${filename}
done

if grep -q "${1}" ${filename}
then
    printf "Found %s!\n" "${1}"
fi

rm ${filename}
