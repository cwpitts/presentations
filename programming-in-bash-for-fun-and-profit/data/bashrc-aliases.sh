#!/bin/bash

# Shorthand for commands
alias a='ls'

# Really good for shortening commands with flags
alias q='ls -slap'

# Or renaming them
alias bat='acpi'
