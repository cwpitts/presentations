#!/bin/bash

# Python-style for x in y loop
# $(seq 10) expands to 1 2 3 4 5 6 7 8 9 10
for i in $(seq 10)
do
    printf "%d\n" "${i}"
done
