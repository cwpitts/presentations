#!/bin/bash

target=5
# Getting input from command line arguments
num=${1}

# Comparisons are [ <statement> ]
if [ ${num} -gt ${target} ]
then
    printf "greater!\n"
elif [ ${num} -lt ${target} ]
then
    printf "less!\n"
else
    printf "equal!\n"
fi # Note we end with 'fi'
