#!/bin/bash

# Conditionals: spaces a must
x=0
if [ ${x} -eq 1 ]
then
    printf "x is the right number!\n"
fi

# Variables: spaces a no-no
y = "foobar"
