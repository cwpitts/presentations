#!/bin/bash

x=10

# To do math, use ((<math>))
((x--))
((x=x-1))

# Or <var>=$((<math>))
x=$((x-1))

# Or let
let x-- # That's a double dash there
printf "%d\n" "${x}"
