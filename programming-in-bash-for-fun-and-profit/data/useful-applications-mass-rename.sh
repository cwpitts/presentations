#!/bin/bash

oldExt="${1}"
newExt="${2}"

# Use this regex globbing instead of $(ls)
for file in *.${oldExt}
do
    # Get base filename (strip extension)
    f=$(printf "%s" "${file}" | cut -d '.' -f 1)
    
    # Move to new file and change extension
    mv "${file}" "${f}.${newExt}"
done
