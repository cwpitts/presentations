#!/bin/bash

# Arguments are passed in in an array
# Arbitrary access with ${<index>}

printf "The first argument: %s\n" "${1}"
printf "The second argument: %s\n" "${2}"
printf "Did we get a third? %s\n" "${3}"

# Get all of the values in an array with ${@}
printf "%s\n" "${@}"
