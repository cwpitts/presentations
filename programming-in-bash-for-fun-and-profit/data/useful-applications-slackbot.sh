#!/bin/bash

msg="${1}"
slackteam="${2}"

curl -X POST --data-urlencode\
     "payload={\"text\":\"${msg}\"}"\
     "${slackteam}"
