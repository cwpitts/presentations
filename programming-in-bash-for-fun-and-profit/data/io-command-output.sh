#!/bin/bash

# Use $(<command>) to capture command output
# Note the distinct lack of spaces
c=$(ls)
printf "%s\n" "${c}"
