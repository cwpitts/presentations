#!/bin/bash
var="foobar"

# This will not do what we want
printf '%s\n' '${var}'

# This will
printf "%s\n" "${var}"

# So will this, as it happens
printf '%s\n' "${var}"
