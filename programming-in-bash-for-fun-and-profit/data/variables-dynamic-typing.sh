#!/bin/bash

# x is a string
x="foobar"
printf "%s\n" "${x}"

# x is now a number!
x=7
printf "%d\n" "${x}"
# Or a string?
printf "%s\n" "${x}"
