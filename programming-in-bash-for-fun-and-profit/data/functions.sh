#!/bin/bash

# Could also do: function f
# Parens make it look nice
function f()
{
    printf "function f got: %s\n" "${1}"
}

# Call like it's on the command line
# <function> <arguments>
f "${1}"
