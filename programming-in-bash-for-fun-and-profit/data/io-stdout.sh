#!/bin/bash

function even_or_odd()
{
    if (( $((${1} % 2)) == 0 ))
    then
	printf "Even!\n"
    else
	printf "Odd!\n"
    fi
}

res=$(even_or_odd ${1})
printf "%s\n" "${res}"
