#!/bin/bash

target=$((RANDOM % ${1}))

guess=-1

while [ ${guess} -ne ${target} ]
do
    read -p "guess: " guess
    
    if [ -z "${guess}" ]
    then
	guess=-1
    fi

    if [ ${guess} -eq -1 ]
    then
	printf "give me a guess!\n"
    elif [ ${guess} -lt ${target} ]
    then
	printf "higher!\n"
    elif [ ${guess} -gt ${target} ]
    then
	printf "lower!\n"
    fi
done

printf "You win!\n"
