#!/bin/bash

x="foobar"

printf "|%s|\n" "${x}"

# -z "${var}" returns 0 if var is
# undefined or the empty string
if [ ! -z "${y}" ]
then
    printf "|%s|\n" "${y}"
fi
