#!/bin/bash

# Correct
var=1

# Incorrect (whitespace not allowed)
var = 1

# Access values with $
var2=${var1}

printf "%d\n" "${var2}"
