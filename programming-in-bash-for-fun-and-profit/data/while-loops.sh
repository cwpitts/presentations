#!/bin/bash

k=1
while ((k < 10))
do
    printf "%d\n" "${k}"
    ((k=k+1)) # Note the ((math)) setup here
    # Could also do let k++
done
