# Presentations
## Overview
This is a public-facing repository for presentations that I give. Usually each folder will have two things:
1. Slides
2. Code examples (where applicable)

## Topics
### Command line
Supercharge Your Bash Prompt
### Scripting
Programming in Bash for Fun and Profit
### Web development
Build Awesome Websites with Flask Python
