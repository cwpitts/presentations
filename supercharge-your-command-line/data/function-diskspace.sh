# Creating diskspace command
function diskspace
{
    # Get argument for directory
    checkdir="${1}"
    if [[ -z ${checkdir} ]]
    then
	# Default to current directory
	checkdir="$(pwd)"
    fi

    # Go to directory
    builtin cd "${checkdir}"

    # Get temporary log file
    tmpfile=$(mktemp)
    printf "Creating temporary log in %s\n" "${tmpfile}"

    # Check diskspace
    printf "Checking diskspace in %s...\n" "${checkdir}"
    du -S -h --max-depth=1 | sort -n -r > ${tmpfile}
    less ${tmpfile}

    # Return to previous directory
    cd -
}
