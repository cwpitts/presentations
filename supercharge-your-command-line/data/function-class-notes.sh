# Open notes in Emacs for editing
function class()
{
    emacs "${HOME}/Documents/school/winter-2017/${1,,}/notes.org" & > /dev/null
}
