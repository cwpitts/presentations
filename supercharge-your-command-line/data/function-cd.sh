# Changing behavior of cd
function cd
{
    # This will pass all the arguments on the
    # command line (the "$@") to the 'cd' builtin,
    # and then execute an 'ls' on the current directory
    builtin cd "$@" && ls
}
