# LaTeX templating function
function latex-template()
{
    # Require arguments
    if [ $# -ne 1 ]
    then
	printf "error: requires filename\n"
	return
    fi

    # The argument is the document name
    docname=${1}

    # Copy the template documents over
    printf "Copying templates..."
    cp "${HOME}/.templates/latex/docname.tex" "${docname}.tex"
    cp "${HOME}/.templates/latex/makefile" makefile

    # Configure the makefile
    printf "Configuring makefile..."
    sed -i -e "s/docname/${docname}/g" makefile

    printf "Done\n"
}
