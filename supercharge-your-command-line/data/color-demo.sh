#!/bin/bash

FG=38
BG=48

for area in ${FG} ${BG}
do
    for color in {0..255}
    do
	printf "\e[${area};5;${color}m ${color}\e[0m "
	if [[ $((color % 6)) == 0 ]]
	then
	    printf '\n'
	fi
    done
done
printf '\e[0m \n'
